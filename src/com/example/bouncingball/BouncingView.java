package com.example.bouncingball;

import android.app.ActionBar.LayoutParams;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.view.KeyEvent;
import android.view.View;

public class BouncingView extends View{
	 private int xMin = 0; 
	 private int xMax;  
	 private int yMin = 0;  
	 private int yMax;
	 private float ballRadius = 40;
	 private float ballX = ballRadius + 20;
	 private float ballY = ballRadius + 40; 
	 private float ballSpeedX = 5; 
	 private float ballSpeedY = 3;  
	 private RectF ballBounds;
	 private Paint paint; 
	 
	 public BouncingView(Context context){
		 super(context);  
		 ballBounds = new RectF();  
		 paint = new Paint();  
		 
		 this.setFocusable(true);  
		 this.requestFocus();
		 
	 }
	 @Override  
	 protected void onDraw(Canvas canvas) {
		 ballBounds.set(ballX-ballRadius, ballY-ballRadius, ballX+ballRadius, ballY+ballRadius);  
		 paint.setColor(Color.GREEN);  
		 canvas.drawOval(ballBounds, paint);  
		 update();  
		 try {    
			 Thread.sleep(60);
			 invalidate();
		 }catch(InterruptedException e){
			 invalidate();
		 }
	 }
	 private void update() {  
		 ballSpeedY+=20;
		 ballY += ballSpeedY; 
		 if (ballX + ballRadius > xMax) { 
			 ballSpeedX = -ballSpeedX; 
			 ballX = xMax-ballRadius; 
		 }else if (ballX - ballRadius < xMin) {
			 ballSpeedX = -ballSpeedX; 
			 ballX = xMin+ballRadius;   
		 }
		 if (ballY + ballRadius > yMax) {
			 ballSpeedY = -ballSpeedY;
			 ballY = yMax - ballRadius; 
		 }else if (ballY - ballRadius < yMin) {
			 ballSpeedY = -ballSpeedY;
			 ballY = yMin + ballRadius;
		 }
	 }
	 
	 public void onSizeChanged(int w, int h, int oldW, int oldH) { 
		 xMax = w-1;  
		 yMax = h-1;  
	 }
}
